package cn.chowa.ejyy.complain;

import cn.chowa.ejyy.common.CodeException;
import cn.chowa.ejyy.common.Constants;
import cn.chowa.ejyy.common.RequestData;
import cn.chowa.ejyy.common.annotation.VerifyCommunity;
import cn.chowa.ejyy.common.utils.AuthUtil;
import cn.chowa.ejyy.dao.ComplainRepository;
import cn.chowa.ejyy.dao.FitmentQuery;
import cn.chowa.ejyy.dao.FitmentRepository;
import cn.chowa.ejyy.model.entity.Complain;
import cn.chowa.ejyy.model.entity.Fitment;
import cn.dev33.satoken.annotation.SaCheckRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import static cn.chowa.ejyy.common.Constants.code.FITMENT_CREATE_FAIL;
import static cn.chowa.ejyy.common.Constants.complain.SUBMIT_COMPLAIN_STEP;
import static cn.chowa.ejyy.common.Constants.fitment.USER_SUBMIT_APPLY_STEP;

@RestController("complainCreate")
@RequestMapping("/pc/complain")
public class create {

    @Autowired
    private ComplainRepository complainRepository;

    /**
     * 投诉建议登记
     */
    @SaCheckRole(Constants.RoleName.TSJY)
    @VerifyCommunity(true)
    @PostMapping("/create")
    public Map<?, ?> create(@RequestBody RequestData data) {
        Long wechat_mp_user_id = data.getLong("wechat_mp_user_id", false, "^\\d+$");
        int type = data.getInt("type", true, "^1|2$");
        int category = data.getInt("category", true, "^1|2|3|4|5|6|7$");
        String description = data.getStr("description", true, 5, 200);
        List<String> complain_imgs = data.getArray("complain_imgs", s -> Pattern.matches("^\\/complain\\/[a-z0-9]{32}\\.(jpg|jpeg|png)$", s));

        Complain complain = complainRepository.save(
                Complain.builder()
                        .propertyCompanyUserId(wechat_mp_user_id == null ? null : AuthUtil.getUid())
                        .wechatMpUserId(wechat_mp_user_id)
                        .communityId(data.getCommunityId())
                        .type(type)
                        .category(category)
                        .description(description)
                        .complain_imgs(complain_imgs == null ? null : String.join("#", complain_imgs))
                        .step(SUBMIT_COMPLAIN_STEP)
                        .createdAt(System.currentTimeMillis())
                        .build()
        );

        return Map.of(
                "id", complain.getId()
        );
    }

}
