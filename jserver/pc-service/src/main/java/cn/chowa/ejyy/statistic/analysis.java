package cn.chowa.ejyy.statistic;

import cn.chowa.ejyy.common.Constants;
import cn.chowa.ejyy.common.RequestData;
import cn.chowa.ejyy.common.annotation.VerifyCommunity;
import cn.chowa.ejyy.dao.*;
import cn.chowa.ejyy.model.entity.*;
import cn.dev33.satoken.annotation.SaCheckRole;
import cn.hutool.core.date.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/pc/statistic")
public class analysis {

    @Autowired
    private BuildingInfoRepository buildingInfoRepository;
    @Autowired
    private PetRepository petRepository;
    @Autowired
    private RepairRepository repairRepository;
    @Autowired
    private ComplainRepository complainRepository;
    @Autowired
    private MoveCarRepository moveCarRepository;
    @Autowired
    private VistorRepository vistorRepository;
    @Autowired
    private NoticeToUserRepository noticeToUserRepository;
    @Autowired
    private BuildingQuery buildingQuery;


    @SaCheckRole(Constants.RoleName.ANYONE)
    @VerifyCommunity(true)
    @PostMapping("/analysis")
    public Map<?, ?> getAnalysis(@RequestBody RequestData param) {
        long houseTotal = buildingInfoRepository.countByCommunityIdAndType(
                param.getCommunityId(), Constants.building.HOUSE);

        long houseBindingTotal = buildingQuery.getBindingTotal(param.getCommunityId(),
                Constants.building.HOUSE, Constants.status.BINDING_BUILDING);

        long carportTotal = buildingInfoRepository.countByCommunityIdAndType(
                param.getCommunityId(), Constants.building.CARPORT);

        long carportBindingTotal = buildingQuery.getBindingTotal(param.getCommunityId(),
                Constants.building.CARPORT, Constants.status.BINDING_BUILDING);

        long warehouseTotal = buildingInfoRepository.countByCommunityIdAndType(
                param.getCommunityId(), Constants.building.WAREHOUSE);

        long warehouseBindingTotal = buildingQuery.getBindingTotal(param.getCommunityId(),
                Constants.building.WAREHOUSE, Constants.status.BINDING_BUILDING);

        long merchantTotal = buildingInfoRepository.countByCommunityIdAndType(
                param.getCommunityId(), Constants.building.MERCHANT);

        long merchantBindingTotal = buildingQuery.getBindingTotal(param.getCommunityId(),
                Constants.building.MERCHANT, Constants.status.BINDING_BUILDING);

        long garageTotal = buildingInfoRepository.countByCommunityIdAndType(
                param.getCommunityId(), Constants.building.GARAGE);

        long garageBindingTotal = buildingQuery.getBindingTotal(param.getCommunityId(),
                Constants.building.GARAGE, Constants.status.BINDING_BUILDING);

        long ownerTotal = buildingQuery.getOwnerTotal(param.getCommunityId(), Constants.status.BINDING_BUILDING);

        long carTotal = buildingQuery.getCarTotal(Constants.status.TRUE, param.getCommunityId(), Constants.building.CARPORT);

        int petTotal = petRepository.countByCommunityId(param.getCommunityId());

        long start = DateUtil.beginOfDay(new Date()).getTime() - 1000 * 6 * 24 * 60 * 60;
        long end = DateUtil.endOfDay(new Date()).getTime();

        List<Repair> repairList = repairRepository.findByCommunityIdAndCreatedAtBetween(param.getCommunityId(), start, end);
        List<Complain> complainList = complainRepository.findByCommunityIdAndCreatedAtBetween(param.getCommunityId(), start, end);
        List<MoveCar> moveCarList = moveCarRepository.findByCommunityIdAndCreatedAtBetween(param.getCommunityId(), start, end);
        List<Pet> petList = petRepository.findByCommunityIdAndCreatedAtBetween(param.getCommunityId(), start, end);
        List<Vistor> visitorList = vistorRepository.findByCommunityIdAndCreatedAtBetween(param.getCommunityId(), start, end);
        List<NoticeToUser> noticeToUserList = noticeToUserRepository.findByCommunityIdAndCreatedAtBetween(param.getCommunityId(), start, end);

        var result = new HashMap<>();
        result.putAll(
                Map.of(
                        "house_total", houseTotal,
                        "house_binding_total", houseBindingTotal,
                        "carport_total", carportTotal,
                        "carport_binding_total", carportBindingTotal,
                        "warehouse_total", warehouseTotal,
                        "warehouse_binding_total", warehouseBindingTotal,
                        "merchant_total", merchantTotal,
                        "merchant_binding_total", merchantBindingTotal,
                        "garage_total", garageTotal,
                        "garage_binding_total", garageBindingTotal
                ));
        result.putAll(
                Map.of(
                        "ower_total", ownerTotal,
                        "car_total", carTotal,
                        "pet_total", petTotal,
                        "repairList", repairList,
                        "complainList", complainList,
                        "moveCarList", moveCarList,
                        "petList", petList,
                        "vistorList", visitorList,
                        "noticeList", noticeToUserList)
        );
        return result;
    }
}
