package cn.chowa.ejyy.fitment;

import cc.iotkit.jql.ObjData;
import cn.chowa.ejyy.common.Constants;
import cn.chowa.ejyy.common.PagedData;
import cn.chowa.ejyy.common.RequestData;
import cn.chowa.ejyy.common.annotation.VerifyCommunity;
import cn.chowa.ejyy.dao.FitmentQuery;
import cn.dev33.satoken.annotation.SaCheckRole;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController("fitmentList")
@RequestMapping("/pc/fitment")
public class list {

    @Autowired
    private FitmentQuery fitmentQuery;

    @SaCheckRole(Constants.RoleName.ZXDJ)
    @VerifyCommunity(true)
    @PostMapping("/list")
    public PagedData<ObjData> getList(@RequestBody RequestData data) {
        int step = data.getInt("step", false, "^1|2|3|4$", -1);
        int is_return_cash_deposit = data.getInt("is_return_cash_deposit", false, "^1|0$", -1);

        return PagedData.of(data.getPageNum(), data.getPageSize(),
                fitmentQuery.getFitments(step, is_return_cash_deposit, data.getCommunityId(),
                        data.getPageSize(), data.getPageNum())
                , fitmentQuery.getFitmentsCount(step, is_return_cash_deposit, data.getCommunityId()));
    }

}
