package cn.chowa.ejyy.config;

import cc.iotkit.jql.JqlQueryConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.server.standard.ServerEndpointExporter;

@Configuration
public class AutoBeanConfiguration {

    @Bean
    public JqlQueryConfiguration getJqlQueryConfiguration() {
        return new JqlQueryConfiguration("cn.chowa.ejyy.dao");
    }

    @Bean
    public ServerEndpointExporter serverEndpointExporter() {
        return new ServerEndpointExporter();
    }
}
