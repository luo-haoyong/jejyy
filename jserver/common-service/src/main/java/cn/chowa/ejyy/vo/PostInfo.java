package cn.chowa.ejyy.vo;

import cc.iotkit.jql.ObjData;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PostInfo {

    private String department;

    private String job;

    @JsonProperty("community_list")
    private List<ObjData> communityList;

    @JsonProperty("default_community_id")
    private long defaultCommunityId;

    @JsonProperty("wechat_payment")
    private int wechatPayment;
}
