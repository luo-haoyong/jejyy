package cn.chowa.ejyy.user;


import cn.chowa.ejyy.common.Constants;
import cn.chowa.ejyy.common.RequestData;
import cn.chowa.ejyy.common.annotation.VerifyCommunity;
import cn.chowa.ejyy.common.utils.PhoneUtil;
import cn.chowa.ejyy.dao.WechatMpUserRepository;
import cn.dev33.satoken.annotation.SaCheckRole;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/mp/user")
public class bind_phone {

    @Autowired
    private WechatMpUserRepository wechatMpUserRepository;

    @VerifyCommunity(true)
    @PostMapping("/bind_phone")
    public Map<?, ?> agree(@RequestBody RequestData data) {
        String code = data.getStr("code", true, "^[0-9a-zA-Z]{32}$");
        String encryptedData = data.getStr("encryptedData", true, null);
        String iv = data.getStr("iv", true, null);
        //todo 解密数据取得手机号
        String phone = "131121212121";
        //todo 更新手机号

        return Map.of(
                "phone", PhoneUtil.hide(phone)
        );
    }

}
