package cn.chowa.ejyy.model.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * 疫情防控
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "ejyy_epidemic")
public class Epidemic {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @JsonProperty("community_id")
    private long communityId;

    @JsonProperty("wechat_mp_user_id")
    private Long wechatMpUserId;

    @JsonProperty("building_id")
    private long buildingId;

    /**
     * 码颜色
     * 1 绿色 2 黄色 3 红色
     */
    @JsonProperty("tour_code")
    private int tourCode;

    /**
     * 温度
     */
    private float temperature;

    /**
     * 返回地
     */
    @JsonProperty("return_hometown")
    private int returnHometown;

    /**
     * 返回省份
     */
    @JsonProperty("return_from_province")
    private String returnFromProvince;

    /**
     * 返回城市
     */
    @JsonProperty("return_from_city")
    private String returnFromCity;

    /**
     * 返回街道
     */
    @JsonProperty("return_from_district")
    private String returnFromDistrict;

    @JsonProperty("created_by")
    private long createdBy;

    @JsonProperty("created_at")
    private long createdAt;

}
