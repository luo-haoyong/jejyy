package cn.chowa.ejyy.dao.question;

import cn.chowa.ejyy.model.entity.question.Questionnaire;
import org.springframework.data.jpa.repository.JpaRepository;

public interface QuestionnaireRepository extends JpaRepository<Questionnaire, Long> {
}
