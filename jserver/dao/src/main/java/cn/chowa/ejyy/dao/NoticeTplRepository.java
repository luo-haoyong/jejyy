package cn.chowa.ejyy.dao;

import cn.chowa.ejyy.model.entity.NoticeTpl;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NoticeTplRepository extends JpaRepository<NoticeTpl, Long> {
}
