package cn.chowa.ejyy.common.utils;

public class PhoneUtil {

    public static String hide(String phone) {
        return phone == null ? null : phone.substring(0, 3) + "****" + phone.substring(7, 11);
    }

}
