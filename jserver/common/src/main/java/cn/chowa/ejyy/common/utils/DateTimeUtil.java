package cn.chowa.ejyy.common.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @ClassName DateTimeUtil
 * @Description TODO
 * @Author ironman
 * @Date 16:24 2022/8/25
 */
public class DateTimeUtil {


    /**
     * 时间戳按指定格式转化为日期（String）
     * @param timestamp
     * @param pattern
     * @return
     */
    public static String convertTimestamp2Date(Long timestamp, String pattern) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        return simpleDateFormat.format(new Date(timestamp));
    }

}
